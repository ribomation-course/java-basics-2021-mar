package ribomation;

import ribomation.domain.Person;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Loads a CSV file person and prints them out, filtered.
 */
public class PersonsApp {
    public static void main(String[] args) throws IOException {
        var app = new PersonsApp();
        var filename = args.length == 0 ? "./data/persons.csv" : args[0];
        app.run(filename);
    }

    /**
     * Loads, filters and prints out the result
     * @param filename  name of file
     * @throws IOException
     */
    void run(String filename) throws IOException {
        final int[] lineCnt = {1};
        Files.lines(Path.of(filename))
                .skip(1)
                .map(payload -> payload.split(";"))
                .map(payload -> new Person(payload[0], payload[1], payload[2], payload[3]))
                .filter(Person::isFemale)
                .filter(person -> 30 <= person.getAge() && person.getAge() <= 40)
                .filter(person -> person.getPostCode() < 20_000)
                .forEach(person -> System.out.printf("%d) %s%n", lineCnt[0]++, person))
        ;
    }
}

