package ribomation.domain;

import java.util.StringJoiner;

/**
 * Represents a single person
 */
public class Person {
    final String name;
    final boolean female;
    final int age;
    final int postCode;

    /**
     * Constructor that take string values as parameters and converts them into primitive values
     * @param name      full name
     * @param female    true if female
     * @param age       numeric positive age
     * @param postCode  numeric value within [10000, 99999]
     */
    public Person(String name, String female, String age, String postCode) {
        this(
                name,
                female.equals("Female"),
                Integer.parseInt(age),
                Integer.parseInt(postCode)
        );
    }

    /**
     * Standard constructor
     * @param name      its name
     * @param female    true if female
     * @param age       its age
     * @param postCode  the postal code
     */
    public Person(String name, boolean female, int age, int postCode) {
        this.name = name;
        this.female = female;
        this.age = age;
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("female=" + female)
                .add("age=" + age)
                .add("postCode=" + postCode)
                .toString();
    }

    public String getName() {
        return name;
    }

    public boolean isFemale() {
        return female;
    }

    public int getAge() {
        return age;
    }

    public int getPostCode() {
        return postCode;
    }
}
