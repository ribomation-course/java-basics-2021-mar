package ribomation.persons;

import java.util.StringJoiner;

public class Person {
    private final String name;
    private final int age;
    private final boolean female;
    private final int postCode;

    public Person(String name, int age, boolean female, int postCode) {
        this.name = name;
        this.age = age;
        this.female = female;
        this.postCode = postCode;
    }

    public Person(String name, String age, String female, String postCode) {
        this(name, Integer.parseInt(age), female.equals("Female"), Integer.parseInt(postCode));
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("age=" + age)
                .add("female=" + female)
                .add("postCode=" + postCode)
                .toString();
    }

    public String getName() { return name; }
    public int getAge() { return age; }
    public boolean isFemale() { return female; }
    public int getPostCode() { return postCode; }
}
