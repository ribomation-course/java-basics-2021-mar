package ribomation.persons;

import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws Exception {
        var app = new Main();
        var csvFile = "/persons.csv";
        var lines = app.openResource(csvFile);
        var persons = app.load(lines);
        var result = app.select(persons);
        app.printText(result);
        app.printJson(result);
    }

    void printText(List<Person> persons) {
        persons.forEach(System.out::println);
    }

    void printJson(List<Person> persons) {
        var gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        System.out.println(gson.toJson(persons));
    }

    Stream<String> openResource(String csvFile) {
        var is = this.getClass().getResourceAsStream(csvFile);
        if (is == null) {
            throw new RuntimeException("cannot find csv file " + csvFile);
        }
        return new BufferedReader(new InputStreamReader(is)).lines();
    }

    List<Person> load(Stream<String> lines) throws IOException {
        return lines
                .skip(1)
                .map(csv -> csv.split(","))
                .map(field -> new Person(field[0], field[1], field[2], field[3]))
                // .peek(person -> System.out.println(person))
                .collect(Collectors.toList());
    }

    List<Person> select(List<Person> persons) {
        return persons.stream()
                .filter(Person::isFemale)  // (Type obj) -> obj.method()  ==> Type::method
                .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                .filter(p -> p.getPostCode() < 20_000)
                .collect(Collectors.toList());
    }

}
