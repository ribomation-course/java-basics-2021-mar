package se.ribomation;

import se.ribomation.domain.Shape;

import java.util.Comparator;

public class AreaComparator implements Comparator<Shape> {
    @Override
    public int compare(Shape lhs, Shape rhs) {
        return Float.compare(lhs.area(), rhs.area());
    }

}
