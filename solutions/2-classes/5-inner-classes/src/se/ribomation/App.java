package se.ribomation;

import se.ribomation.domain.*;

//import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

import static java.util.Arrays.sort;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.run();
    }

    void run() throws Exception {
        Shape[] shapes = create(5);

//        Arrays.sort(shapes, new AreaComparator().reversed());

//        sort(shapes, new Comparator<Shape>() {
//            @Override
//            public int compare(Shape lhs, Shape rhs) {
//                return Float.compare(rhs.area(), lhs.area());
//            }
//        });

        sort(shapes, Shape.orderByAreaAsc());
        for (var shape : shapes) {
            System.out.printf("%s= %.2f%n", shape.name(), shape.area());
        }
        System.out.println("------");
        sort(shapes, Shape.orderByAreaAsc().reversed());
        for (var shape : shapes) {
            System.out.printf("%s= %.2f%n", shape.name(), shape.area());
        }
        System.out.println("------");
        sort(shapes, Shape.orderByNameAsc());
        for (var shape : shapes) {
            System.out.printf("%s= %.2f%n", shape.name(), shape.area());
        }
        System.out.println("------");
        sort(shapes);
        for (var shape : shapes) {
            System.out.printf("%s= %.2f%n", shape.name(), shape.area());
        }


        System.out.println("=============");
        var r1 = new Rect(5, 10);
        var r2 = r1.clone();
        System.out.printf("r1 == r2 ? %b%n", (r1 == r2));
        System.out.printf("r1.equals(r2) ? %b%n", r1.equals(r2));
        System.out.printf("r1.hash=%d, r2.hash=%d%n", r1.hashCode(), r2.hashCode());
    }

    Shape[] create(int n) {
        var shapes = new Shape[n];
        for (int k = 0; k < shapes.length; k++) {
            shapes[k] = create();
        }
        return shapes;
    }

    Shape create() {
        switch (r.nextInt(3)) {
            case 0:
                return new Rect(r.nextInt(10) + 1, r.nextInt(10) + 1);
            case 1:
                return new Triang(r.nextInt(10) + 1, r.nextInt(10) + 1);
            case 2:
                return new Circle(r.nextInt(10) + 1);
        }
        throw new RuntimeException("invalid type id");
    }

    private final Random r = new Random();

}
