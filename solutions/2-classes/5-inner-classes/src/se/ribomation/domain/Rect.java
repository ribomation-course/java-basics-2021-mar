package se.ribomation.domain;

public class Rect extends Shape implements Cloneable {
    private final int width;
    private final int height;

    public Rect(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rect rect = (Rect) o;

        if (width != rect.width) return false;
        return height == rect.height;
    }

    @Override
    public int hashCode() {
        int result = width;
        result = 31 * result + height;
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Rect(width, height);
    }

    @Override
    public String name() {
        return String.format("Rectangle(%d, %d)", width, height);
    }

    @Override
    public float area() {
        return width * height;
    }
}
