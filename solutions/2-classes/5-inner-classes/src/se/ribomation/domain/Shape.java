package se.ribomation.domain;

import java.util.Comparator;

public abstract class Shape implements Comparable<Shape> {
    public abstract String name();
    public abstract float area();

    @Override
    public int compareTo(Shape rhs) {
        return  Float.compare(this.area(), rhs.area());
    }

    public static Comparator<Shape> orderByAreaAsc() {
        return new Comparator<Shape>() {
            @Override
            public int compare(Shape lhs, Shape rhs) {
                return Float.compare(lhs.area(), rhs.area());
            }
        };
    }

    public static Comparator<Shape> orderByNameAsc() {
        return new Comparator<Shape>() {
            @Override
            public int compare(Shape lhs, Shape rhs) {
                return lhs.name().compareTo(rhs.name());
            }
        };
    }

}
