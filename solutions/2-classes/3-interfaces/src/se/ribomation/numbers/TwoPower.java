package se.ribomation.numbers;

import java.io.Serializable;

public class TwoPower implements Numbers, Serializable {
    private int nextVal = 1;

    @Override
    public int next() {
        try {
            return nextVal;
        } finally {
            nextVal *= 2;
        }
    }
}
