package se.ribomation.numbers;

public interface Numbers {
    int next();

    default int[] create(int n) {
        var arr = new int[n];
        for (int k = 0; k < arr.length; k++) {
            arr[k] = next();
        }
        return arr;
    }
}
