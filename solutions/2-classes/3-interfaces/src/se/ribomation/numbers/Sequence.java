package se.ribomation.numbers;

public class Sequence implements Numbers {
    private int nextVal = 1;

    @Override
    public int next() {
        try {
            return nextVal;
        } finally {
            ++nextVal;
        }
    }
}
