package se.ribomation.numbers;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run("SEQUENCE", new Sequence());
        app.run("TWOPOWER", new TwoPower());
    }

    private void run(String name, Numbers num) {
        System.out.print(name + ": ");
        var n = 10;
        while (n-- > 0) {
            System.out.printf("%d ", num.next());
        }
        System.out.println();
    }
}
