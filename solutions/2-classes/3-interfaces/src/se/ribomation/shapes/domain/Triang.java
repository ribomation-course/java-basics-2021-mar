package se.ribomation.shapes.domain;

public class Triang extends Shape {
    private final int base;
    private final int height;

    public Triang(int base, int height) {
        this.base = base;
        this.height = height;
    }

    @Override
    public String name() {
        return String.format("Triangle(%d, %d)", base, height);
    }

    @Override
    public float area() {
        return base * height / 2F;
    }
}
