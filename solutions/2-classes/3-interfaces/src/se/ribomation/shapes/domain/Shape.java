package se.ribomation.shapes.domain;

public abstract class Shape {
    public abstract String name();
    public abstract float area();
}
