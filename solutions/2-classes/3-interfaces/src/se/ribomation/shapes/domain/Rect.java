package se.ribomation.shapes.domain;

public class Rect extends Shape {
    protected final int width;
    protected final int height;

    public Rect(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public String name() {
        return String.format("Rectangle(%d, %d)", width, height);
    }

    @Override
    public float area() {
        return width * height;
    }

}
