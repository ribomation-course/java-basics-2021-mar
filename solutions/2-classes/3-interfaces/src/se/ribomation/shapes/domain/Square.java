package se.ribomation.shapes.domain;

public class Square extends Rect {
    public Square(int side) {
        super(side, side);
    }

    @Override
    public float area() {
        var side = super.width;
        return side * side;
    }

    @Override
    public String name() {
        return String.format("Square(%d)", width);
    }

    public int perimeter() {
        return 4 * width;
    }
}
