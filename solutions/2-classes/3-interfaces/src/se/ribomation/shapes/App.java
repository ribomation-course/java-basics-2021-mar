package se.ribomation.shapes;

import se.ribomation.shapes.domain.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

/**
 * Main entry point
 */
public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        Shape[] shapes = create(10);
        Arrays.sort(shapes, new AreaComparator().reversed());
        for (var shape : shapes) {
            System.out.printf("%s= %.2f%n", shape.name(), shape.area());

            if (shape instanceof Square) {
                //(C) void* ptr = malloc(sizeof(Person))
                //    Person* p = (Person*)ptr,
                //(C++) if (typeid(shape) == Square) ....
                Square sq = (Square) shape;
                System.out.printf("  perimeter=%d%n", sq.perimeter());
            }
        }
    }

    /**
     * Generates a list of shape objects with random data
     * @param n  the number of shapes
     * @return array of shapes
     */
    Shape[] create(int n) {
        var shapes = new Shape[n];
        for (int k = 0; k < shapes.length; k++) {
            shapes[k] = create();
        }
        return shapes;
    }

    Shape create() {
        switch (r.nextInt(4)) {
            case 0:
                return new Rect(r.nextInt(10) + 1, r.nextInt(10) + 1);
            case 1:
                return new Triang(r.nextInt(10) + 1, r.nextInt(10) + 1);
            case 2:
                return new Circle(r.nextInt(10) + 1);
            case 3:
                return new Square(r.nextInt(10) + 1);
        }
        throw new RuntimeException("invalid type id");
    }

    private final Random r = new Random();

    public static class AreaComparator implements Comparator<Shape> {
        @Override
        public int compare(Shape lhs, Shape rhs) {
    //        var leftArea = lhs.area();
    //        var rightArea = rhs.area();
    //        if (leftArea < rightArea) {
    //            return +1;
    //        }
    //        if (leftArea > rightArea) {
    //            return -1;
    //        }
    //        return 0;

            return Float.compare(lhs.area(), rhs.area());
        }

    }
}
