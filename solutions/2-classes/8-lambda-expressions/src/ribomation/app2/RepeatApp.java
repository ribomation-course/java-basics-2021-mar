package ribomation.app2;

import java.util.ArrayList;
import java.util.function.Consumer;

public class RepeatApp {
    public static void main(String[] args) {
        var app = new RepeatApp();
        app.run1();
        app.run2();
    }

    void repeat(int N, Consumer<Integer> expr) {
        for (var k = 1; k <= N; ++k) expr.accept(k);
    }

    void run1() {
        repeat(5, x -> System.out.printf("%d) Java is Cool%n", x));
    }

    void run2() {
        var lst = new ArrayList<String>();
        repeat(10, n -> lst.add("person-" + n));
        System.out.println("lst = " + lst);
    }

}
