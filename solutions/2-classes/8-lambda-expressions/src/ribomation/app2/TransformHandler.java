package ribomation.app2;

public interface TransformHandler<T> {
    T compute(T n);
}
