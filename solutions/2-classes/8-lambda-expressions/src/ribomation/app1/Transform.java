package ribomation.app1;
import java.util.Arrays;
import java.util.function.UnaryOperator;

public class Transform {
    static <T> T[] transform(T[] arr, UnaryOperator<T> f) {
        for (var k = 0; k < arr.length; ++k) arr[k] = f.apply(arr[k]);
        return arr;
    }

    public static void main(String[] args) {
        var numbers = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.printf("(1) %s%n", Arrays.toString(numbers));

        System.out.printf("(2) %s%n", Arrays.toString(transform(numbers, n -> n * n)));

        var fn = new UnaryOperator<Integer>() {
            @Override
            public Integer apply(Integer n) { return n * 10; }
        };
        System.out.printf("(3) %s%n", Arrays.toString(transform(numbers, fn)));
    }
}
