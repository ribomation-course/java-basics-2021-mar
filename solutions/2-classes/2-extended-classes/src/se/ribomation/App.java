package se.ribomation;

import se.ribomation.domain.Circle;
import se.ribomation.domain.Rect;
import se.ribomation.domain.Shape;
import se.ribomation.domain.Triang;

import java.util.Random;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        Shape[] shapes = create(50);
        for (var shape : shapes) {
            System.out.printf("%s= %.2f%n", shape.name(), shape.area());
        }
    }

    Shape[] create(int n) {
        var shapes = new Shape[n];
        for (int k = 0; k < shapes.length; k++) {
            shapes[k] = create();
        }
        return shapes;
    }

    Shape create() {
        switch (r.nextInt(3)) {
            case 0:
                return new Rect(r.nextInt(10) + 1, r.nextInt(10) + 1);
            case 1:
                return new Triang(r.nextInt(10) + 1, r.nextInt(10) + 1);
            case 2:
                return new Circle(r.nextInt(10) + 1);
        }
        throw new RuntimeException("invalid type id");
    }

    private final Random r = new Random();
}
