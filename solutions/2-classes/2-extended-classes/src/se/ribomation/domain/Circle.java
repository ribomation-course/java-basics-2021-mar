package se.ribomation.domain;

public class Circle extends Shape {
    private final int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public String name() {
        return String.format("Circle(%d)", radius);
    }

    @Override
    public float area() {
        return (float) (Math.PI * radius * radius);
    }
}
