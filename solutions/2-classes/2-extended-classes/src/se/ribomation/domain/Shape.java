package se.ribomation.domain;

public abstract class Shape {
    public abstract String name();
    public abstract float area();
}
