package se.ribomation.domain;

public class Rect extends Shape {
    private final int width;
    private final int height;

    public Rect(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public String name() {
        return String.format("Rectangle(%d, %d)", width, height);
    }

    @Override
    public float area() {
        return width * height;
    }
}
