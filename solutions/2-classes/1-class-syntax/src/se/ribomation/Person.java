package se.ribomation;

import java.util.StringJoiner;

public class Person {
    private final String name;
    private final int age;
    private Car car;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("age=" + age)
                .add("car=" + car)
                .toString();
    }
}
