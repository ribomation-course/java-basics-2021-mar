package se.ribomation;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    private void run() {
        var car = new Car("ABC123", "Volvo Amazon");
        System.out.printf("car: %s%n", car);

        var person = new Person("Nisse", 42);
        person.setCar(car);
        System.out.printf("pers: %s%n", person);

        person.setCar(null);
        System.out.printf("pers: %s%n", person);

        car.setModel("Volvo PV");
        System.out.printf("car: %s%n", car);
    }
}
