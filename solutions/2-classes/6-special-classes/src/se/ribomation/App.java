package se.ribomation;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run(40, 2);
    }

    void run(int lhs, int rhs) {
//        Operator[] ops = {
//                Operator.add, Operator.sub, Operator.mul, Operator.div
//        };
        for (var op : Operator.values()) {
            System.out.printf("%d %s %d = %d%n", lhs, op, rhs, op.eval(lhs, rhs));
        }

        System.out.printf("sym: %s%n", Operator.valueOf("div"));
    }
}
