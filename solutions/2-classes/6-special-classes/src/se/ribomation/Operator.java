package se.ribomation;

public enum Operator {
    add("+"), sub("-"), mul("*"), div("/"), mod("%");

    public final String symbol;
    Operator(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }

    public int eval(int lhs, int rhs) {
        switch (this) {
            case add:
                return lhs + rhs;
            case sub:
                return lhs - rhs;
            case mul:
                return lhs * rhs;
            case div:
                return lhs / rhs;
            case mod:
                return lhs % rhs;
        }
        return 0;
    }

//    public String symbol() {
//        switch (this) {
//            case add:
//                return "+";
//            case sub:
//                return "-";
//            case mul:
//                return "*";
//            case div:
//                return "/";
//        }
//        return "";
//    }
}
