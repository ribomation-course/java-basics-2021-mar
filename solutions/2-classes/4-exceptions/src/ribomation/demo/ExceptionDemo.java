package ribomation.demo;

public class ExceptionDemo {
    public static void main(String[] args) {
        var app = new ExceptionDemo();
//        app.run_default();
        app.run_catch();
        app.run_catch();
    }

    void run_default() {
        System.out.println("ExceptionDemo.run_default ENTER");
        var bizop = new BusinessOperations();
        bizop.doit();
    }

    void run_catch() {
        System.out.println("ExceptionDemo.run_catch ENTER");
        try {
            var bizop = new BusinessOperations();
            bizop.doit();
        } catch (Exception e) {
            System.out.println("Ooops, got an error");
            e.printStackTrace(System.out);
            System.out.println("will continue execute");
        } finally {
            System.out.println("ExceptionDemo.run_catch EXIT");
        }
    }
}
