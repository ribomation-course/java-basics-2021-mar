package ribomation.demo;

public class BusinessOperations {
    public void doit() {
        System.out.println("BusinessObject.doit ENTER");
        try {
            step1();
        } finally {
            System.out.println("BusinessObject.doit EXIT");
        }
    }

    private void step1() {
        System.out.println("BusinessObject.step1 ENTER");
        try {
            step2();
        } finally {
            System.out.println("BusinessObject.step1 EXIT");
        }
    }

    private void step2() {
        System.out.println("BusinessObject.step2 ENTER");
        try {
            step3();
        } finally {
            System.out.println("BusinessObject.step2 EXIT");
        }
    }

    private void step3() {
        System.out.println("BusinessObject.step3 ENTER");
        try {
            Object obj = null;
            System.out.println("*** BANG ***");
            System.out.println(obj.toString());
        } finally {
            System.out.println("BusinessObject.step3 EXIT");
        }
    }
}
