package ribomation.demo;

public class ArrApp {
    public static void main(String[] args) {
        var app = new ArrApp();
        try {
            app.run();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("array error: " + e.getMessage());
        }
    }

    private void run() {
        var nums = new int[5];
        for (int num : nums) {
            System.out.printf("%d ", num);
        }
        System.out.println();

        var n0 = nums[0];
        System.out.println("n0 = " + n0);

        var n6 = nums[6];
        System.out.println("n6 = " + n6);
    }
}
