package se.ribomation;

public class SimpleQueue<T> {
    private final T[] storage;
    private int size = 0;
    private int putIndex = 0;
    private int getIndex = 0;

    public SimpleQueue(int capacity) {
        storage = (T[]) new Object[capacity];
        // C:
        // int* storage = (int*) malloc(capacity * sizeof(int));
    }

    public boolean empty() {
        return size == 0;
    }

    public boolean full() {
        return size == storage.length;
    }

    public void put(T x) {
        if (full()) {
            throw new RuntimeException("full");
        }
        storage[putIndex++] = x;
        if (putIndex == storage.length) putIndex = 0;
        ++size;
    }

    public T get() {
        var x = storage[getIndex++];
        if (getIndex == storage.length) getIndex = 0;
        --size;
        return x;
    }
}
