package se.ribomation;

import java.util.Calendar;
import java.util.Date;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run_Integer(10);
        app.run_String(5);
        app.run_Date(4);
    }

    void run_Integer(int n) {
        var q = new SimpleQueue<Integer>(n);
        for (var k=1; !q.full(); ++k) q.put(k);
        drain(q);
    }

    void run_String(int n) {
        var q = new SimpleQueue<String>(n);
        for (var k=1; !q.full(); ++k) q.put("str-" + k);
        drain(q);
    }

    void run_Date(int n) {
        var q = new SimpleQueue<Date>(n);
        var cal = Calendar.getInstance();
        for (var k=1; !q.full(); ++k) {
            q.put(cal.getTime());
            cal.add(Calendar.DATE, 2);
        }
        drain(q);
    }

    <Type> void drain(SimpleQueue<Type> q) {
        while (!q.empty()) System.out.printf("%s, ", q.get());
        System.out.println();
    }

}
