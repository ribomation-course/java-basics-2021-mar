package ribomation;

import ribomation.domain.Student;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.analyze(app.create());
    }

    Object create() {
        return new Student("Nisse Hult", 42, "Knäckebröhult's Byskola");
    }

    void analyze(Object obj) {
        analyze(obj.getClass());
    }

    void analyze(Class cls) {
        if (cls == null) return;
        System.out.printf("-- %s --%n", cls.getName());

        for (Method m : cls.getDeclaredMethods()) {
            System.out.printf("  %s(%s): %s%n",
                    m.getName(),
                    Stream.of(m.getParameterTypes())
                            .map(Class::getSimpleName)
                            .collect(Collectors.joining(", ")),
                    m.getReturnType().getSimpleName());
        }

        for (Field f : cls.getDeclaredFields()) {
            System.out.printf("  %s: %s <%s>%n",
                    f.getName(),
                    f.getType().getSimpleName(),
                    Modifier.isPrivate(f.getModifiers()) ? "private" : "public"
            );
        }

        for (Class i : cls.getInterfaces()) {
            System.out.printf("  %s%n", i);
        }

        analyze(cls.getSuperclass());
    }

}
