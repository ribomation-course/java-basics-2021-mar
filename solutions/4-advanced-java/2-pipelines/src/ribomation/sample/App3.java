package ribomation.sample;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class App3 {
    public static void main(String[] args) throws IOException {
        Path p = Paths.get("./src/ribomation/App3.java");
        List<String[]> result = Files.lines(p)
                .map(line -> line.split("\\s+"))
                .collect(Collectors.toList());
        result.forEach(arr -> System.out.println(Arrays.toString(arr)));
    }
}
