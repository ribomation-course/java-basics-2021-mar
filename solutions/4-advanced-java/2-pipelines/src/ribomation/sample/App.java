package ribomation.sample;

import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    void run() {
        boolean result = Stream
                .of("abcdef", "hej", "tjabba")
                .map(word -> word.toUpperCase())
                .peek(elem -> System.out.printf("* %s%n", elem))
//                .filter(word -> word.length() > 3)
//                .peek(elem -> System.out.printf("** %s%n", elem))
                .allMatch(w -> w.length() == 6);

        System.out.printf("result: %s%n", result);
    }
}
