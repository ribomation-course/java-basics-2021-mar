package ribomation.sample;

import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;

public class App2 {
    public static void main(String[] args) {
        System.out.println(
        IntStream.rangeClosed(1, 10).boxed()
                .peek(System.out::println)
                .reduce(1, (acc, elem) -> acc * elem / 0)
        );
               // .ifPresent(n -> System.out.println(n));

//                .collect(groupingBy(n -> n % 5))
        //  .forEach((key,val) -> System.out.printf("%s: %s%n", key, val));

    }
}
