package ribomation.words;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.*;

public class WordCloudApp {
    public static void main(String[] args) throws Exception {
        var file = "./data/musketeers.txt";
        var app = new WordCloudApp();
        app.load(file);
    }

    void load(String file) throws IOException {
        var startTime = System.nanoTime();
        var wordSep = Pattern.compile("[^a-zA-Z]+");

        var words = Files
                .lines(Path.of(file))
                .flatMap(wordSep::splitAsStream)
                .filter(word -> word.length() > 4)
                .collect(groupingBy(String::toLowerCase, counting()))
                .entrySet().stream()
                .sorted((lhs, rhs) -> (int) (rhs.getValue() - lhs.getValue()))
                .limit(100)
                .collect(toList());

        var maxFreq = words.get(0).getValue();
        var minFreq = words.get(words.size() - 1).getValue();
        var maxFont = 200.0;
        var minFont = 25.0;
        var scale = (maxFont - minFont) / (maxFreq - minFreq);
        var prefix = "<html><body>";
        var suffix = "</body></html>";
        var r = new Random();
        Supplier<String> color = () -> format("%02x%02x%02x", r.nextInt(256), r.nextInt(256), r.nextInt(256));
        Function<Long, Integer> scaled = value -> (int) (value * scale + minFont);
        Comparator<String> shuffle = (_a, _b) -> r.nextBoolean() ? -1 : +1;

        var html = words.stream()
                .map(pair -> format("<span style='font-size:%dpx; color:#%s'>%s</span>",
                        scaled.apply(pair.getValue()),
                        color.get(),
                        pair.getKey()))
                .sorted(shuffle)
                .collect(joining(lineSeparator(), prefix, suffix));

        var outfile = Path.of("./out/words.html");
        Files.writeString(outfile, html);
        System.out.printf("written %s%n", outfile);

        var endTime = System.nanoTime();
        System.out.printf("elapsed %.3f seconds", (endTime - startTime) * 1E-9);
    }

}
