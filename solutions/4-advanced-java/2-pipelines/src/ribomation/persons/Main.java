package ribomation.persons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws Exception {
        var app = new Main();
        var dataFile = Path.of("./data/persons.csv");
        if (!Files.isReadable(dataFile)) {
            throw new RuntimeException("file not found: " + dataFile);
        }

        var persons = app.load(dataFile);
        var result = app.select(persons);
        result.forEach(System.out::println);
    }

    List<Person> load(Path file) throws IOException {
        return Files
                .lines(file)
                .skip(1)
                .map(csv -> csv.split(","))
                .map(field -> new Person(field[0], field[1], field[2], field[3]))
               // .peek(person -> System.out.println(person))
                .collect(Collectors.toList());
    }

    List<Person> select(List<Person> persons) {
        return persons.stream()
                .filter(Person::isFemale)  // (Type obj) -> obj.method()  ==> Type::method
                .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                .filter(p -> p.getPostCode() < 20_000)
                .collect(Collectors.toList());
    }

}
