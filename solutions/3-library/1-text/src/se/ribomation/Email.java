package se.ribomation;

public class Email {
    public static boolean valid(String email) {
        return check(email);
    }

    public static boolean invalid(String email) {
        return !check(email);
    }

    public static boolean check(String email) {
        if (email == null || email.trim().length() == 0) {
            return false;
        }

        var parts = email.split("@");
        if (parts.length != 2) {
            return false;
        }

        return checkName(parts[0]) && checkDomain(parts[1]);
    }

    static boolean checkName(String name) {
        if (!name.matches("^[a-zA-Z0-9.-]+$")) return false;
        if (!Character.isLetter(name.charAt(0))) return false;
        if (name.charAt(name.length() - 1) == '.') return false;
        return true;
    }

    static boolean checkDomain(String domain) {
        if (!domain.matches("^[a-zA-Z.]+$")) return false;
        if (!Character.isLetter(domain.charAt(0))) return false;
        if (domain.charAt(domain.length() - 1) == '.') return false;

        var tld = domain.substring(domain.lastIndexOf('.') + 1);
        return tld.length() == 2 || tld.length() == 3;
    }

}
