package se.ribomation;

import java.util.function.Predicate;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.test(invalid_emails, Email::invalid);
        app.test(valid_emails, Email::valid);
    }

    void test(String[] emails, Predicate<String> evaluate) {
        for (var email : emails) {
            boolean passed = evaluate.test(email);
            System.out.printf("%-25s --> %s%n",
                    email,
                    passed ? "passed" : "failed");
        }
    }

    static String[] valid_emails = {
            "jens@ribomation.se",
            "anna.conda@gmail.com"
    };

    static String[] invalid_emails = {
            null,
            "",
            "    ",
            "jens@ribomation@se",
            "anna.conda.gmail.com",
            "anna.conda.@gmail.com",
            "anna.conda@gmail.com.",
            "42anna.conda@gmail.com",
            "anna#conda@gmail.com",
            "anna.conda@gmail.info",
            "anna.conda@gmail42.se",
    };

}
