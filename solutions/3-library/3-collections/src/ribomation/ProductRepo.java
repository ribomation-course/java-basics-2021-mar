package ribomation;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProductRepo {
    private final Random r = new Random();

    public List<Product> create(int n) {
        var result = new ArrayList<Product>(n);
        for (var k = 0; k < n; ++k) result.add(create());
        return result;
    }

    public Product create() {
        String name  = names[r.nextInt(names.length)];
        float  price = Math.abs((float) (r.nextGaussian() * 100 + 50));
        int    count = r.nextInt(10);
        return new Product(name, price, count);
    }

    private static final String[] names = {
            "apple", "banana", "coco nut", "date plum", "kiwi", "orange", "peach"
    };
}
