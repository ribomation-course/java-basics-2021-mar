package ribomation;

import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        var list = new ProductRepo().create(10);
        print(list);
        sortedByPriceDesc(list);
        byHashSet(list);
        byTreeSet(list);
        byTreeSetPriceOrder(list);
        byTreeMap(list);
        removeEventCount(list);
    }

    private void print(List<Product> list) {
        System.out.println("--- by insertion order ---");
        list.forEach(System.out::println);
    }

    private void sortedByPriceDesc(List<Product> list) {
        System.out.println("--- sorted by price desc ---");
//            list.sort((lhs, rhs) -> {
//                var leftPrice = lhs.getPrice();
//                var rightPrice = rhs.getPrice();
//                if (leftPrice < rightPrice) return +1;
//                if (leftPrice > rightPrice) return -1;
//                return 0;
//            });
        list.sort((lhs, rhs) -> Float.compare(rhs.getPrice(), lhs.getPrice()));
        list.forEach(System.out::println);
    }

    private void byHashSet(List<Product> list) {
        System.out.println("--- hash-set (unordered) ---");
        var hashSet = new HashSet<>(list);
        hashSet.forEach(System.out::println);
    }

    private void byTreeSet(List<Product> list) {
        System.out.println("--- tree-set by name (duplicate names removed) ---");
        var treeSet = new TreeSet<>(list);
        treeSet.forEach(System.out::println);
    }

    private void byTreeSetPriceOrder(List<Product> list) {
        System.out.println("--- tree-set by price desc ---");
        var treeSet = new TreeSet<>((Product lhs, Product  rhs) -> Float.compare(rhs.getPrice(), lhs.getPrice()));
        treeSet.addAll(list);
        treeSet.forEach(System.out::println);
    }

    private void byTreeMap(List<Product> list) {
        System.out.println("--- tree-map by name (duplicate names removed) ---");
        var map = new TreeMap<String, Product>();
        list.forEach(p -> map.put(p.getName(), p));
        map.forEach((name, product) -> System.out.printf("%s: %s%n", name, product));
    }

    private void removeEventCount(List<Product> list) {
        System.out.println("--- removed even count ---");
        list.removeIf(p -> p.getCount() % 2 == 0);
        list.forEach(System.out::println);
    }
}
