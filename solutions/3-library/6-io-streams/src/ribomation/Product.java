package ribomation;

import java.io.*;
import java.util.Objects;
import java.util.StringJoiner;

public class Product implements Comparable<Product>, Serializable {
     String name;
     double price;
     int count;

/*
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(name);
        out.writeDouble(price);
        out.writeInt(count);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        name = in.readUTF();
        price = in.readDouble();
        count = in.readInt();
    }
*/

    public Product() {
    }

    public static Product fromCSV(String csv, String sep) {
        var fields = csv.split(sep);
        return new Product(fields[0], fields[1], fields[2]);
    }

    public Product(String name, String count, String price) {
        this(name, Integer.parseInt(count), Double.parseDouble(price));
    }

    public Product(String name, int count, double price) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Product.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("price=" + price)
                .add("count=" + count)
                .toString();
    }

    @Override
    public int compareTo(Product that) {
        return this.name.compareTo(that.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 && count == product.count && name.equals(product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, count);
    }
}
