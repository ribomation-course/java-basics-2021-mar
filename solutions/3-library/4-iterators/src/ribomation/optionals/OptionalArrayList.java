package ribomation.optionals;

import java.util.ArrayList;
import java.util.Optional;

public class OptionalArrayList<E> extends ArrayList<E> {
    public Optional<E> lookup(int index) {
        try {
            return Optional.ofNullable(super.get(index));
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
