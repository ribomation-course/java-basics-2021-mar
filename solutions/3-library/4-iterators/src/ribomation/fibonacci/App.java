package ribomation.fibonacci;

public class App {
    public static void main(String[] args) {
        App app = new App();

        app.run_for_each(10);
        app.run_while(10);
        app.run_forEach(10);
    }

    void run_for_each(int count) {
        System.out.printf("fib(%d): ", count);
        for (var f : new Fibonacci(count)) System.out.printf("%d ", f);
        System.out.println();
    }

    void run_while(int count) {
        System.out.printf("fib(%d): ", count);
        var fib = new Fibonacci(count);
        var it = fib.iterator();
        while (it.hasNext()) {
            var f = it.next();
            System.out.printf("%d ", f);
        }
        System.out.println();
    }

    void run_forEach(int count) {
        System.out.printf("fib(%d): ", count);
        new Fibonacci(count).forEach(obj -> System.out.print(obj + " "));
        System.out.println();
    }
}
