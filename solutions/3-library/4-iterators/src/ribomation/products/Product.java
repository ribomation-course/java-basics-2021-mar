package ribomation.products;

import java.util.Locale;
import java.util.Objects;

public class Product {
    private String name;
    private double price;
    private int    count;

    public Product(String name, double price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH,
                "Product{%s, SEK %.2f, %d items}",
                name, price, count);
    }

    @Override
    public boolean equals(Object that) {
        if (this.getClass() != that.getClass()) return false;
        return Objects.deepEquals(this, that);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, count);
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }
}
