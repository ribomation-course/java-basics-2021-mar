import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        var app = new GuessNumber();
        // app.init();
        app.run();
        app.printResult();
    }

    final int maxGuesses = 5;
    final int secret= new Random().nextInt(10) + 1;
    final Scanner in= new Scanner(System.in);
    int numGuesses = 0;

    // void init() {
    //     secret = new Random().nextInt(10) + 1;
    //     in     = new Scanner(System.in);
    // }

    void run() {
        for (numGuesses = 1; numGuesses <= maxGuesses; ++numGuesses) {
            var guess = readGuess();
            if (guess == secret) {
                System.out.println("!! correct !!");
                return;
            }
            printHint(guess);
        }
        --numGuesses;
        System.out.println("** game over **");
    }

    void printResult() {
        System.out.printf("secret     : %d%n", secret);
        System.out.printf("num guesses: %d%n", numGuesses);
    }

    void printHint(int guess) {
        if (guess < secret) System.out.println("too low");
        else System.out.println("too high");
    }

    int readGuess() {
        System.out.print("Make a guess: ");
        return in.nextInt();
    }
}
