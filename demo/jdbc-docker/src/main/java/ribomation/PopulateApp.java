package ribomation;

import ribomation.domain.Person;
import ribomation.mappers.CsvMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class PopulateApp {
    public static void main(String[] args) throws Exception {
        var app = new PopulateApp();
        app.run();
    }

    void run() throws SQLException, IOException {
        try (var con = openDB("/jdbc.properties")) {
            var persons = loadCSV("./data/persons.csv");
            System.out.printf("loaded %d CSV person records%n", persons.size());

            createTable(con);
            populateDB(con, persons);
        }
    }

    List<Person> loadCSV(String filename) throws IOException {
        var mapper = new CsvMapper(";");
        return Files
                .lines(Path.of(filename))
                .skip(1)
                .map(mapper::fromCsv)
                .collect(Collectors.toList());
    }

    Connection openDB(String jdbcResource) throws SQLException, IOException {
        var props = new Properties();
        try (var is = getClass().getResourceAsStream(jdbcResource)) {
            props.load(is);
        }
        var host = props.getProperty("host");
        var port = Integer.parseInt(props.getProperty("port"));
        var db = props.getProperty("database");
        var usr = props.getProperty("user");
        var pwd = props.getProperty("password");
        var url = String.format("jdbc:mysql://%s:%d/%s?serverTimezone=UTC", host, port, db);
        return DriverManager.getConnection(url, usr, pwd);
    }

    void createTable(Connection con) throws SQLException {
        try (var stmt = con.createStatement()) {
            stmt.execute("drop table if exists persons;");

            var tblSql = "create table persons (" +
                    "id  integer primary key auto_increment," +
                    "name  varchar(32)," +
                    "female  boolean not null," +
                    "age  integer not null," +
                    "post_code integer not null" +
                    ");";
            stmt.execute(tblSql);
        }
    }

    void populateDB(Connection con, List<Person> persons) throws SQLException {
        var cnt = 0;
        var sql = "insert into persons (name,female,age,post_code) values (?,?,?,?)";
        try (var stmt = con.prepareStatement(sql)) {
            for (Person person : persons) {
                stmt.setString(1, person.getName());
                stmt.setBoolean(2, person.isFemale());
                stmt.setInt(3, person.getAge());
                stmt.setInt(4, person.getPostCode());
                var rc = stmt.executeUpdate();
                if (rc != 1) System.out.printf("unexpected: %d <%s>%n", rc, person);
                ++cnt;
            }
        }
        System.out.printf("populated db with %d records%n", cnt);
    }

}
