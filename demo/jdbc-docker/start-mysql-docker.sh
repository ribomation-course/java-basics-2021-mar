#!/usr/bin/env bash
set -eux

name=mysql
container=mysql/mysql-server:5.7
port=3306
rootPasswd=foobar
user=nisse
passwd=hult
db=persondb


docker run --name $name \
  --detach \
  --publish $port:$port \
  --env MYSQL_ROOT_PASSWORD=$rootPasswd \
  --env MYSQL_USER=$user \
  --env MYSQL_PASSWORD=$passwd \
  --env MYSQL_DATABASE=$db \
  $container


