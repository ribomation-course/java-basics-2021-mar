package ribomation.ddl;

import ribomation.ddl.api.Column;
import ribomation.ddl.api.Table;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GenerateDDL {
    public static final GenerateDDL instance = new GenerateDDL();

    private GenerateDDL() {
    }

    public String generate(String cls) throws ClassNotFoundException {
        return generate(Class.forName(cls));
    }

    public String generate(Class cls) {
        Annotation table = cls.getAnnotation(Table.class);
        if (table == null) throw new RuntimeException("Missing @Table");

        return String.format("CREATE TABLE %s (%n%s%n);",
                cls.getSimpleName().toLowerCase() + "s",
                format(extractColumns(cls))
        );
    }

    private List<ColumnDef> extractColumns(Class cls) {
        return Stream.of(cls.getDeclaredFields())
                .filter((f) -> f.getAnnotation(Column.class) != null)
                .map(ColumnDef::new)
                .collect(Collectors.toList())
                ;
    }

    private String format(List<ColumnDef> columns) {
        final int maxWidth = columns.stream()
                .map(ColumnDef::getName)
                .mapToInt(String::length)
                .max()
                .orElse(0);

        return columns.stream()
                .map(c -> String.format("  %-" + maxWidth + "s  %s", c.getName(), c.getType()))
                .collect(Collectors.joining("," + System.lineSeparator()))
                ;
    }

}
