package ribomation.ddl;

import ribomation.ddl.api.Column;
import ribomation.ddl.api.Default;
import ribomation.ddl.api.NotNull;
import ribomation.ddl.api.PrimaryKey;

import java.lang.reflect.Field;

/**
 * Represents a single SQL column
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2017-01-20
 */
public class ColumnDef {
    String  name;
    String  type;
    boolean pk;
    boolean autoIncrement;
    boolean notNull;
    String  defval;

    public ColumnDef(Field f) {
        name = f.getName().toLowerCase();
        type = f.getAnnotation(Column.class).value().toUpperCase();
        notNull = (f.getAnnotation(NotNull.class) != null);
        defval = (f.getAnnotation(Default.class) != null) ? f.getAnnotation(Default.class).value() : null;

        PrimaryKey primaryKey = f.getAnnotation(PrimaryKey.class);
        if ((primaryKey != null)) {
            pk = true;
            autoIncrement = primaryKey.autoIncrement();
        }
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return String.format("%s%s%s%s%s",
                type,
                pk ? " PRIMARY KEY" : "",
                autoIncrement ? " AUTO-INCREMENT" : "",
                notNull ? " NOT NULL" : "",
                defval != null ? " DEFAULT " + defval : "");
    }
}
