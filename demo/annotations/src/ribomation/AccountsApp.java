package ribomation;

import ribomation.ddl.GenerateDDL;

public class AccountsApp {
    public static void main(String[] args) throws Exception {
        AccountsApp app = new AccountsApp();
        app.run("ribomation.domain.Account");
        app.run("ribomation.domain.Invoice");
    }

    void run(String clsName) throws Exception {
        System.out.printf("--- DDL SQL ---\n%s%n",
                GenerateDDL.instance.generate(clsName));
    }

}
