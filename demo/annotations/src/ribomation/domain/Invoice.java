package ribomation.domain;

import ribomation.ddl.api.*;

import java.util.Date;

@Table
public class Invoice {
    @Column("integer")
    @PrimaryKey(autoIncrement = true)
    private long id;

    @Column("datetime")
    @Default("today()")
    private Date invoice_Date;

    @Column("varchar(64)")
    @NotNull
    private String customer;

    @Column("decimal(12,0)")
    @NotNull
    private long amount;

    public Invoice(String customer, Date date, long amount) {
        this.customer = customer;
        this.invoice_Date = date;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "customer='" + customer + '\'' +
                ", invoice_Date=" + invoice_Date +
                ", amount=" + amount +
                '}';
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Date getInvoice_Date() {
        return invoice_Date;
    }

    public void setInvoice_Date(Date invoice_Date) {
        this.invoice_Date = invoice_Date;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
